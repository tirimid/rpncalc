# rpncalc

## Introduction
rpncalc, or (R)everse (P)olish (N)otation (Calc)ulator, is a command line tool
for evaluating simply RPN expressions. It can either be used in an interactive
mode, or to evaluate a single command fed to it through standard input. It is
extremely simple and minimal, and I will add features as I need them.

## Dependencies
System / software dependencies are:
* A Linux system (or any POSIX-compatible system)
* A shell environment for program execution

## Management
* To build the program, run `make`
* To install the program, run `make install`
* To uninstall the program from the system, run `make uninstall`

## Usage
After installation, enter the interactive mode by running `rpncalc`. Then, you
simply enter the expression you wish to evaluate and press enter. To quit, just
type "q" on its own and press enter.

To evaluate a single expression in a more script-convenient way, you should pipe
the expression into `rpncalc -e`, the `-e` specifies that only one expression
will be evaluated and then rpncalc will immediately terminate. For example:

```
$ echo "3 4 + 2 *" | rpncalc -e
14
```

## Contributing
This project is not worth contributing to. I will not be accepting pull
requests from anyone but myself.
