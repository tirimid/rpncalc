#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <optional>
#include <stack>
#include <string>
#include <vector>

extern "C" {
#include <unistd.h>
}

enum tokentype {
	TT_VALUE = 0,

	TT_OP_BEGIN__,
	TT_BINOP_BEGIN__ = TT_OP_BEGIN__,
	TT_OP_ADD = TT_BINOP_BEGIN__,
	TT_OP_SUB,
	TT_OP_MUL,
	TT_OP_DIV,
	TT_BINOP_END__ = TT_OP_DIV,
	TT_OP_END__ = TT_BINOP_END__,
};

struct token {
	std::string val;
	unsigned char type;
};

static int validatetok(token const &tok);
static std::optional<std::vector<token>> lex(std::string const &src);
static std::optional<double> eval(std::vector<token> const &toks);
static std::string trim(std::string const &s);
static void usage(std::string const &pname);

static bool flag_e = false;

int
main(int argc, char const *argv[])
{
	int ch;
	while ((ch = getopt(argc, (char *const *)argv, "eh")) != -1) {
		switch (ch) {
		case 'e':
			flag_e = true;
			break;
		case 'h':
			usage(argv[0]);
			return 0;
		default:
			return 1;
		}
	}

	for (;;) {
		std::string ln;
		std::getline(std::cin, ln);
		ln = trim(ln);

		if (ln == "q")
			break;

		std::optional<std::vector<token>> toks = lex(ln);
		if (!toks) {
			std::cerr << "lexing failed!\n";
			if (flag_e)
				return 1;
			else
				continue;
		}

		std::optional<double> res = eval(*toks);
		if (!res) {
			std::cerr << "evaluation failed!\n";
			if (flag_e)
				return 1;
			else
				continue;
		}

		std::cout << *res << '\n';

		if (flag_e)
			break;
	}

	return 0;
}

static int
validatetok(token const &tok)
{
	// ensure all values take one of the following forms:
	// * [0-9]+
	// * [0-9]+.[0-9]+

	if (tok.type != TT_VALUE)
		return 0;

	if (tok.val[0] == '.') {
		std::cerr << "leading decimal point in value!\n";
		return 1;
	}

	if (tok.val[tok.val.length() - 1] == '.') {
		std::cerr << "trailing decimal point in value!\n";
		return 1;
	}

	if (std::count(tok.val.begin(), tok.val.end(), '.') > 1) {
		std::cerr << "multiple decimal points in value!\n";
		return 1;
	}

	return 0;
}

static std::optional<std::vector<token>>
lex(std::string const &src)
{
	std::vector<token> toks;

	std::string valaccum;
	auto voidaccum = [&] {
		if (valaccum.length()) {
			token tok = {
				.val = valaccum,
				.type = TT_VALUE,
			};
			toks.push_back(tok);
			valaccum = "";
		}
	};

	for (char ch : src) {
		if (std::isdigit(ch) || ch == '.') {
			valaccum += ch;
			continue;
		}

		voidaccum();

		if (std::isspace(ch))
			continue;

		static char oplut[] = {'+', '-', '*', '/'};
		unsigned char type = TT_OP_BEGIN__;
		while (type <= TT_OP_END__ && oplut[type - TT_OP_BEGIN__] != ch)
			++type;

		if (type == TT_OP_END__ + 1) {
			std::cerr << "unhandled character: " << ch << "!\n";
			return std::nullopt;
		}

		token tok = {
			.val = "",
			.type = type,
		};

		toks.push_back(tok);
	}

	voidaccum();

	for (token const &tok : toks) {
		if (validatetok(tok))
			return std::nullopt;
	}

	return toks;
}

static std::optional<double>
eval(std::vector<token> const &toks)
{
	std::stack<double> stk;

	for (token const &tok : toks) {
		static std::function<double(double, double)> oplut[] = {
			[](double lhs, double rhs) {return lhs + rhs;},
			[](double lhs, double rhs) {return lhs - rhs;},
			[](double lhs, double rhs) {return lhs * rhs;},
			[](double lhs, double rhs) {return lhs / rhs;},
		};

		if (tok.type == TT_VALUE) {
			stk.push(std::stod(tok.val));
			continue;
		}
		
		if (tok.type >= TT_BINOP_BEGIN__ && tok.type <= TT_BINOP_END__) {
			if (stk.size() < 2) {
				std::cerr << "not enough operands for binop!\n";
				return std::nullopt;
			}

			double rhs = stk.top();
			stk.pop();
			double lhs = stk.top();
			stk.pop();

			stk.push(oplut[tok.type - TT_OP_BEGIN__](lhs, rhs));
		}
	}

	if (!stk.size()) {
		std::cerr << "no result!\n";
		return std::nullopt;
	}

	return stk.top();
}

static std::string
trim(std::string const &s)
{
	// there is a better way to do this with algorithm.
	size_t start = 0;
	while (start < s.length() && std::isspace(s[start]))
		++start;

	size_t end = s.length();
	while (end > 0 && std::isspace(s[end - 1]))
		--end;

	if (start == end)
		return "";

	return s.substr(start, s.length()).substr(0, end - start);
}

static void
usage(std::string const &pname)
{
	std::cout << "rpncalc - simple RPN command line calculator\n";
	std::cout << '\n';
	std::cout << "usage:\n";
	std::cout << '\t' << pname << " [options]\n";
	std::cout << "options:\n";
	std::cout << "\t-e  terminate after evaluating one expression from stdin\n";
	std::cout << "\t-h  display this help information\n";
}
