.PHONY: all install uninstall clean

BIN := rpncalc
INSTBIN := /usr/bin/rpncalc
CPP := g++
CPPFLAGS := -std=c++20 -pedantic -D_POSIX_C_SOURCE=200809

all: $(BIN)

install: all
	cp $(BIN) $(INSTBIN)

uninstall:
	rm -f $(INSTBIN)

clean:
	rm -f $(BIN)

$(BIN): rpncalc.cc
	$(CPP) $(CPPFLAGS) -o $@ $<
